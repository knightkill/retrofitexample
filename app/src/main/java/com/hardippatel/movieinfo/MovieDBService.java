package com.hardippatel.movieinfo;

import com.hardippatel.movieinfo.models.Results;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 *
 * Created by knighkill on 23/06/16.
 * Interface for Retrofit to interpret HTTP API to Callable Methods
 *
 */
public interface MovieDBService {

    /**
     *  Request to get list of top rated movies
     * @param api_key The API key provided by TheMovieDb
     * @return Response to Top rated movies request
     */
    @GET("movie/top_rated")
    Call<Results> getTopRatedMovie(@Query("api_key") String api_key,@Query("page") Integer pageNumber);

    @GET("movie/now_playing")
    Call<Results> getNowPlayingMovie(@Query("api_key") String api_key,@Query("page") Integer pageNumber);
}
