package com.hardippatel.movieinfo;

import com.hardippatel.movieinfo.models.Results;
import com.hardippatel.movieinfo.models.upload.ImageResult;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 *
 * Created by knighkill on 23/06/16.
 * Interface for Retrofit to interpret HTTP API to Callable Methods
 *
 */
public interface ImageUploadService {

    /**
     *  Image to be uploaded
     * @param image Image file
     * @return Response to Top rated movies request
     */
    @Multipart
    @POST("api")
    Call<ImageResult> uploadImage(@Part MultipartBody.Part image);
}
