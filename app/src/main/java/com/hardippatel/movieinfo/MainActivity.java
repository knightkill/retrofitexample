package com.hardippatel.movieinfo;

import android.content.res.ColorStateList;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager pager;
    private PagerAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Adding toolbar to the activity
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Initializing the tablayout
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setTabTextColors(ContextCompat.getColorStateList(this,R.drawable.text_color_tablayout));
        tabLayout.setSelectedTabIndicatorColor(ContextCompat.getColor(this,R.color.colorAccent));

        pager = (ViewPager) findViewById(R.id.pager);

        //Creating Adapter
        adapter = new PagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOffscreenPageLimit(2);

        final TabLayout.Tab topMovies = tabLayout.newTab();
        topMovies.setText("Top Movies");
        tabLayout.addTab(topMovies);

        final TabLayout.Tab nowPlayingMovies = tabLayout.newTab();
        nowPlayingMovies.setText("Now Playing");
        tabLayout.addTab(nowPlayingMovies);

        final TabLayout.Tab imageUploadTab = tabLayout.newTab();
        imageUploadTab.setText("Image Upload");
        tabLayout.addTab(imageUploadTab);

        pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

    }
}
