package com.hardippatel.movieinfo.fragments;


import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.hardippatel.movieinfo.ImageUploadService;
import com.hardippatel.movieinfo.R;
import com.hardippatel.movieinfo.models.Results;
import com.hardippatel.movieinfo.models.upload.ImageResult;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ImageUploadFragment extends Fragment {


    private static final String TAG = ImageUploadFragment.class.getSimpleName();
    private Button imageUploadButton;
    private ImageView downloadedImageImageView;
    private ImageUploadService service;

    public static ImageUploadFragment newInstance() {
        ImageUploadFragment fragment = new ImageUploadFragment();
        Bundle args = new Bundle();
        //Add args
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_image_upload, container, false);
        initViews(view);

        //Initializing Retrofit
        Retrofit retrofit = new Retrofit.Builder()
                //Setting Base url
                .baseUrl("http://uploads.im")
                //Setting converter for Serializing and Deserializing JSON String
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(ImageUploadService.class);


        imageUploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ProgressDialog progressBar =new ProgressDialog(getContext());
                progressBar.setCancelable(true);
                progressBar.setMessage("File loading from drawable ...");
                progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressBar.show();
                File file = getFile();

                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), file);

                final MultipartBody.Part body =
                        MultipartBody.Part.createFormData("upload", file.getName(), requestFile);
                progressBar.setMessage("File uploading...");
                service.uploadImage(body).enqueue(new Callback<ImageResult>() {
                    @Override
                    public void onResponse(Call<ImageResult> call, Response<ImageResult> response) {

                        if(response.isSuccessful()){
                            progressBar.setMessage("File downloading ...");
                            Glide.with(getContext()).load(response.body().getImage().getThumbUrl())
                                    .listener(new RequestListener<String, GlideDrawable>() {
                                        @Override
                                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                            progressBar.dismiss();
                                            return false;
                                        }

                                        @Override
                                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                            progressBar.dismiss();
                                            return false;
                                        }
                                    })
                                    .into(downloadedImageImageView)
                                    .onLoadStarted(getResources().getDrawable(R.mipmap.ic_launcher));

                        }else{
                            Log.e(TAG,"error s");
                        }
                    }

                    @Override
                    public void onFailure(Call<ImageResult> call, Throwable t) {

                        Log.e(TAG,"error " + t.toString());
                    }
                });
            }
        });


        return view;
    }

    private void initViews(View view) {
        imageUploadButton = (Button) view.findViewById(R.id.upload_image_btn);
        downloadedImageImageView = (ImageView) view.findViewById(R.id.downloaded_image_iv);
    }


    public File getFile() {
        Bitmap bitmap = BitmapFactory.decodeResource(this.getResources(), R.drawable.captainplanet);

        File file = new File(getContext().getCacheDir() + File.separator + "drawable");

        boolean doSave = true;
        if (!file.exists()) {
            doSave = file.mkdirs();
        }

        if (doSave) {
            saveBitmapToFile(file,"launcher.png",bitmap,Bitmap.CompressFormat.PNG,100);
        }
        else {
            Log.e(TAG,"Couldn't create target directory.");
        }

        file = new File(getContext().getCacheDir() + File.separator + "drawable/launcher.png" );
        return file;
    }


    public boolean saveBitmapToFile(File dir, String fileName, Bitmap bm,
                                    Bitmap.CompressFormat format, int quality) {

        File imageFile = new File(dir,fileName);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(imageFile);

            bm.compress(format,quality,fos);

            fos.close();

            return true;
        }
        catch (IOException e) {
            Log.e("app",e.getMessage());
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }
        return false;
    }
}
