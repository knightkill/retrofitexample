package com.hardippatel.movieinfo;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.hardippatel.movieinfo.fragments.ImageUploadFragment;
import com.hardippatel.movieinfo.fragments.MoviesFragment;

/**
 * Created by knighkill on 23/06/16.
 */
public class PagerAdapter extends FragmentStatePagerAdapter{

    public static final int TOP_MOVIES_TAB = 0;
    public static final int NOW_PLAYING_TAB = 1;
    public static final int IMAGE_UPLOAD_TAB = 2;

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch(position){
            case TOP_MOVIES_TAB:return MoviesFragment.newInstance(TOP_MOVIES_TAB);
            case NOW_PLAYING_TAB:return MoviesFragment.newInstance(NOW_PLAYING_TAB);
            case IMAGE_UPLOAD_TAB: return ImageUploadFragment.newInstance();
            default: return null;
        }

    }

    @Override
    public int getCount() {
        return 3;
    }



}
