
package com.hardippatel.movieinfo.models.upload;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ImageResult {

    @SerializedName("status_code")
    @Expose
    private Integer statusCode;
    @SerializedName("status_txt")
    @Expose
    private String statusTxt;
    @SerializedName("data")
    @Expose
    private Image image;

    /**
     * 
     * @return
     *     The statusCode
     */
    public Integer getStatusCode() {
        return statusCode;
    }

    /**
     * 
     * @param statusCode
     *     The status_code
     */
    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    /**
     * 
     * @return
     *     The statusTxt
     */
    public String getStatusTxt() {
        return statusTxt;
    }

    /**
     * 
     * @param statusTxt
     *     The status_txt
     */
    public void setStatusTxt(String statusTxt) {
        this.statusTxt = statusTxt;
    }

    /**
     * 
     * @return
     *     The image
     */
    public Image getImage() {
        return image;
    }

    /**
     * 
     * @param image
     *     The image
     */
    public void setImage(Image image) {
        this.image = image;
    }

}
