
package com.hardippatel.movieinfo.models.upload;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Image {

    @SerializedName("img_name")
    @Expose
    private String imgName;
    @SerializedName("img_url")
    @Expose
    private String imgUrl;
    @SerializedName("img_view")
    @Expose
    private String imgView;
    @SerializedName("img_width")
    @Expose
    private String imgWidth;
    @SerializedName("img_height")
    @Expose
    private String imgHeight;
    @SerializedName("img_attr")
    @Expose
    private String imgAttr;
    @SerializedName("img_size")
    @Expose
    private String imgSize;
    @SerializedName("img_bytes")
    @Expose
    private Integer imgBytes;
    @SerializedName("thumb_url")
    @Expose
    private String thumbUrl;
    @SerializedName("thumb_width")
    @Expose
    private Integer thumbWidth;
    @SerializedName("thumb_height")
    @Expose
    private Integer thumbHeight;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("resized")
    @Expose
    private String resized;
    @SerializedName("delete_key")
    @Expose
    private String deleteKey;

    /**
     * 
     * @return
     *     The imgName
     */
    public String getImgName() {
        return imgName;
    }

    /**
     * 
     * @param imgName
     *     The img_name
     */
    public void setImgName(String imgName) {
        this.imgName = imgName;
    }

    /**
     * 
     * @return
     *     The imgUrl
     */
    public String getImgUrl() {
        return imgUrl;
    }

    /**
     * 
     * @param imgUrl
     *     The img_url
     */
    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    /**
     * 
     * @return
     *     The imgView
     */
    public String getImgView() {
        return imgView;
    }

    /**
     * 
     * @param imgView
     *     The img_view
     */
    public void setImgView(String imgView) {
        this.imgView = imgView;
    }

    /**
     * 
     * @return
     *     The imgWidth
     */
    public String getImgWidth() {
        return imgWidth;
    }

    /**
     * 
     * @param imgWidth
     *     The img_width
     */
    public void setImgWidth(String imgWidth) {
        this.imgWidth = imgWidth;
    }

    /**
     * 
     * @return
     *     The imgHeight
     */
    public String getImgHeight() {
        return imgHeight;
    }

    /**
     * 
     * @param imgHeight
     *     The img_height
     */
    public void setImgHeight(String imgHeight) {
        this.imgHeight = imgHeight;
    }

    /**
     * 
     * @return
     *     The imgAttr
     */
    public String getImgAttr() {
        return imgAttr;
    }

    /**
     * 
     * @param imgAttr
     *     The img_attr
     */
    public void setImgAttr(String imgAttr) {
        this.imgAttr = imgAttr;
    }

    /**
     * 
     * @return
     *     The imgSize
     */
    public String getImgSize() {
        return imgSize;
    }

    /**
     * 
     * @param imgSize
     *     The img_size
     */
    public void setImgSize(String imgSize) {
        this.imgSize = imgSize;
    }

    /**
     * 
     * @return
     *     The imgBytes
     */
    public Integer getImgBytes() {
        return imgBytes;
    }

    /**
     * 
     * @param imgBytes
     *     The img_bytes
     */
    public void setImgBytes(Integer imgBytes) {
        this.imgBytes = imgBytes;
    }

    /**
     * 
     * @return
     *     The thumbUrl
     */
    public String getThumbUrl() {
        return thumbUrl;
    }

    /**
     * 
     * @param thumbUrl
     *     The thumb_url
     */
    public void setThumbUrl(String thumbUrl) {
        this.thumbUrl = thumbUrl;
    }

    /**
     * 
     * @return
     *     The thumbWidth
     */
    public Integer getThumbWidth() {
        return thumbWidth;
    }

    /**
     * 
     * @param thumbWidth
     *     The thumb_width
     */
    public void setThumbWidth(Integer thumbWidth) {
        this.thumbWidth = thumbWidth;
    }

    /**
     * 
     * @return
     *     The thumbHeight
     */
    public Integer getThumbHeight() {
        return thumbHeight;
    }

    /**
     * 
     * @param thumbHeight
     *     The thumb_height
     */
    public void setThumbHeight(Integer thumbHeight) {
        this.thumbHeight = thumbHeight;
    }

    /**
     * 
     * @return
     *     The source
     */
    public String getSource() {
        return source;
    }

    /**
     * 
     * @param source
     *     The source
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * 
     * @return
     *     The resized
     */
    public String getResized() {
        return resized;
    }

    /**
     * 
     * @param resized
     *     The resized
     */
    public void setResized(String resized) {
        this.resized = resized;
    }

    /**
     * 
     * @return
     *     The deleteKey
     */
    public String getDeleteKey() {
        return deleteKey;
    }

    /**
     * 
     * @param deleteKey
     *     The delete_key
     */
    public void setDeleteKey(String deleteKey) {
        this.deleteKey = deleteKey;
    }


    @Override
    public String toString() {
        return "Image{" +
                "imgName='" + imgName + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", imgView='" + imgView + '\'' +
                ", imgWidth='" + imgWidth + '\'' +
                ", imgHeight='" + imgHeight + '\'' +
                ", imgAttr='" + imgAttr + '\'' +
                ", imgSize='" + imgSize + '\'' +
                ", imgBytes=" + imgBytes +
                ", thumbUrl='" + thumbUrl + '\'' +
                ", thumbWidth=" + thumbWidth +
                ", thumbHeight=" + thumbHeight +
                ", source='" + source + '\'' +
                ", resized='" + resized + '\'' +
                ", deleteKey='" + deleteKey + '\'' +
                '}';
    }
}
