package com.hardippatel.movieinfo.fragments;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.hardippatel.movieinfo.MovieDBService;
import com.hardippatel.movieinfo.PagerAdapter;
import com.hardippatel.movieinfo.R;
import com.hardippatel.movieinfo.models.MovieResult;
import com.hardippatel.movieinfo.models.Results;
import com.hardippatel.movieinfo.util.Constants;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by knighkill on 23/06/16.
 */
public class MoviesFragment extends Fragment {
    private static final String TAG = MoviesFragment.class.getSimpleName();
    private static final String TAB = "tab";
    private ListView moviesListView;
    private int tab;

    public static MoviesFragment newInstance(int tab) {
        MoviesFragment myFragment = new MoviesFragment();

        Bundle args = new Bundle();
        args.putInt(TAB, tab);
        myFragment.setArguments(args);

        return myFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.movies_fragment_layout, container, false);
        initViews(view);
        tab = getArguments().getInt(TAB);
        //Initializing Retrofit
        Retrofit retrofit = new Retrofit.Builder()
                //Setting Base url
                .baseUrl("https://api.themoviedb.org/3/")
                //Setting converter for Serializing and Deserializing JSON String
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Creating Movie Service
        MovieDBService service = retrofit.create(MovieDBService.class);

        if(tab== PagerAdapter.TOP_MOVIES_TAB) {
            final ProgressDialog progressBar =new ProgressDialog(getContext());
            progressBar.setCancelable(true);
            progressBar.setMessage("Top movies..huh...let me see...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.show();
            service.getTopRatedMovie(Constants.API_KEY, null).enqueue(new Callback<Results>() {
                @Override
                public void onResponse(Call<Results> call, Response<Results> response) {
                    if (response.isSuccessful()) {

                        List<MovieResult> movieresults = response.body().getResults();
                        List<String> movies = new ArrayList<String>();
                        for (MovieResult movieresult : movieresults) {
                            movies.add(movieresult.getOriginalTitle());
                        }
                        if (getActivity() != null) {
                            ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, movies);
                            moviesListView.setAdapter(adapter);
                        }

                    }
                    progressBar.dismiss();
                }

                @Override
                public void onFailure(Call<Results> call, Throwable t) {
                    Log.e(TAG, "error");
                    progressBar.dismiss();
                }
            });
        }else if(tab == PagerAdapter.NOW_PLAYING_TAB){
            final ProgressDialog progressBar =new ProgressDialog(getContext());
            progressBar.setCancelable(true);
            progressBar.setMessage("Current Movies..huh...let me see...");
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.show();
            service.getNowPlayingMovie(Constants.API_KEY, null).enqueue(new Callback<Results>() {
                @Override
                public void onResponse(Call<Results> call, Response<Results> response) {
                    if (response.isSuccessful()) {

                        List<MovieResult> movieresults = response.body().getResults();
                        List<String> movies = new ArrayList<String>();
                        for (MovieResult movieresult : movieresults) {
                            movies.add(movieresult.getOriginalTitle());
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, movies);
                        moviesListView.setAdapter(adapter);

                    } else {
                        Log.e(TAG, "error s");
                    }
                    progressBar.dismiss();
                }

                @Override
                public void onFailure(Call<Results> call, Throwable t) {
                    Log.e(TAG, "error");
                    progressBar.dismiss();
                }
            });
        }
        return view;
    }

    private void initViews(View view) {
        moviesListView = (ListView) view.findViewById(R.id.movies_lv);
    }
}
